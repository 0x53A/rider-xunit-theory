﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace XunitTheoryTest
{
    public class Class1
    {
        public enum MyEnum { A, B, C }

        [CombinatorialData]
        [Theory]
        public void MyTest(MyEnum e, [CombinatorialValues(1,2,3)]int i)
        {
            Thread.Sleep(TimeSpan.FromSeconds(2));
        }
    }
}
